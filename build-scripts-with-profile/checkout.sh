#!/bin/sh
#
# Downloads the latests revision of sources files.
#
# Usage: checkout.sh
#

#fail on error (-e). fail on variable unset (-u).
set -o nounset
set -o errexit

echo ========== Checking out latests revision into sources/ ==========
rm -rf sources
mkdir sources
cd sources
git clone git@bitbucket.org:ideasagiles/mucontacts.git
cd ..
