#!/bin/sh
#
# Builds the project for the given profile.
#
# Usage: build.sh
#

#fail on error (-e). fail on variable unset (-u).
set -o nounset
set -o errexit

#echo ========== Installing Game Reviews Collector ==========
#cd sources_grc/game-reviews-collector
#mvn -Dmaven.test.skip=true clean install
#cd ../..


echo ========== Overwriting config with production values ==========
cp -v application.properties sources/alto-gamer-web/src/main/resources

echo ========== Building ==========
cd sources/alto-gamer-web/
mvn -Dmaven.test.skip=true clean package
