#!/bin/sh
#
# Additional config for Apache Tomcat.
#
# Installation: Copy this file into $CATALINA_HOME/bin
#
set -e
export CATALINA_OPTS="-XX:MaxPermSize=256m"
